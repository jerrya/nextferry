#!python
"""doit calls ferryfeed to download, cleans up, merge multiple ferry feeds

This is an example script of how to call ferryfeed!

The cleaned up feeds are written to their own rss files, and then
another rss feed is created with every departure listed.

Usage:
  doit.py -h | --help
  doit.py [options] [-v...]

  scrape the wsdot ferry rss feeds, clean them up, and write them out.
  the rss feed for each route is written to

Options:
-c --clean               clean directory of prior rss feeds [default: False]
-o --output=OUTPUT       output directory [default: feeds]
-v --verbose             print verbose status messages
-D --debug               print debug messages (equivalent to -vv)
-q --quiet               show only critical errors

"""

import os
import sys
from docopt import docopt

sys.path.append(os.path.join(os.path.dirname(__file__), "wsdot"))

from ferryfeed import ferryfeed

if __name__ == "__main__":

    # handle command line args
    args = docopt(__doc__)

    if args['--verbose'] >= 2:
        args['--debug'] = True

    ferryfeed(
        output=args['--output'],
        clean=args['--clean'],
        debug=args['--debug'],
        verbose=args['--verbose'],
        quiet=args['--quiet']
    )
