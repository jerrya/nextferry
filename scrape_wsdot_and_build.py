#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import datetime
from dateutil import tz
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), "wsdot"))
from ferryfeed import ferryfeed

if not os.path.exists("content/ferries"):
    os.makedirs("content/ferries")

midnight_this_morning = datetime.datetime.today().replace(
    hour=0, minute=0, second=0, microsecond=0)
print("midnight_this_morning: %s" % midnight_this_morning)

utc_zone = tz.gettz('UTC')
pt_zone = tz.gettz('America/Los_Angeles')

midnight_this_morning_pt = midnight_this_morning.replace(tzinfo=pt_zone)
midnight_this_morning_utc = midnight_this_morning_pt.astimezone(utc_zone)
print("midnight_this_morning_pt: %s" % midnight_this_morning_pt)
print("midnight_this_morning_utc: %s" % midnight_this_morning_utc)

no_feeds = True
older_feeds = False
for f in os.listdir("content/ferries"):
    fpath = os.path.join("content/ferries", f)
    if os.path.isfile(fpath):
        no_feeds = False
        fmodified = datetime.datetime.fromtimestamp(os.path.getmtime(fpath))
        fmodified_utc = fmodified.replace(tzinfo=utc_zone)
        if fmodified_utc < midnight_this_morning_utc:
            print("old file: %s[%s]" % (fpath, fmodified_utc))
            older_feeds = True
            break
        else:
            print("newer file: %s[%s]" % (fpath, fmodified_utc))

empty_public = True
if os.path.exists("public"):
    for f in os.listdir("public"):
        empty_public = False
        break

print("empty_public: %s" % empty_public)
print("no_feeds: %s" % no_feeds)
print("older_feeds: %s" % older_feeds)
if no_feeds or older_feeds:
    print("scraping feeds")
    ferryfeed(output="content/ferries")
else:
    print("new feeds already present, skipping the scraping")
print("rebuilding site")
os.system("pelican -s publishconf.py")
