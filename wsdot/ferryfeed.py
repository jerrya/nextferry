#!python

"""ferryfeed downloads, cleans up, merges multiple wsdot ferry feeds

the cleaned up feeds are written to their own rss files, and then
another rss feed is created with every departure listed.

Usage:
  ferryfeed.py -h | --help
  ferryfeed.py [options] [-v...]

  scrape the wsdot ferry rss feeds, clean them up, and write them out.
  the rss feed for each route is written to

Options:
-c --clean               clean directory of prior rss feeds [default: False]
-o --output=OUTPUT       output directory [default: .]
-v --verbose             print verbose status messages
-D --debug               print debug messages (equivalent to -vv)
-q --quiet               show only critical errors

"""

# TODO
# incorporate wsdot api http://www.wsdot.wa.gov/ferries/api/terminals/rest/help
# accesscode is hah! get your own you rascally varmint!

# incorporate route cancellations
# http://www.wsdot.com/ferries/schedule/AddCancelBySimpleRoute.aspx?routeid=8
# (pt - coupeville)

# and wait times
# http://www.wsdot.wa.gov/ferries/api/terminals/rest/help/operations/GetAllTerminalWaitTimes

# and other alerts
# http://www.wsdot.com/Ferries/Schedule/RSSFeeds/RouteAlerts.aspx?departingterm=17&arrivingterm=11

from __future__ import print_function, unicode_literals

from glob import glob
import logging
import os
import sys

from docopt import docopt

from feed import Feed
from ferry import FerrySystem
from settings import args

__version__ = "0.0.dev0"

####################
# FROM STACKOVERFLOW
# http://stackoverflow.com/questions/2183233/how-to-add-a-custom-loglevel-to-pythons-logging-facility


DEBUG_LEVELV_NUM = 9
logging.addLevelName(DEBUG_LEVELV_NUM, "DEBUGV")


def debugv(self, message, *args, **kws):
    """
    Log 'msg % args' with severity 'DEBUG'.

    To pass exception information, use the keyword argument exc_info with
    a true value, e.g.

    logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
    """
    # Yes, logger takes its '*args' as 'args'.
    if self.isEnabledFor(DEBUG_LEVELV_NUM):
        self._log(DEBUG_LEVELV_NUM, "  " + message, args, **kws)


logger = None


def setup_logging():
    global logger
    logging.Logger.debugv = debugv

    handler = logging.StreamHandler()

    # yes, I do leave palimpsests in my code
    # I do use my code as historical record
    # and future tutorial

    # formatter = logging.Formatter(
    #     "%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
    # formatter = logging.Formatter(
    #     "%(name)s %(levelname)s %(message)s")
    formatter = logging.Formatter(
        " %(levelname)-8s %(message)s")
    handler.setFormatter(formatter)

    ####################

    logger = logging.getLogger("ferryfeed")
    logger.addHandler(handler)
    logger.setLevel(logging.WARNING)


setup_logging()


def process_command_line_args():

    # handle command line args

    if args['--debug'] and args['--verbose'] < 2:
        args['--verbose'] = 2

    if args['--verbose'] == 1:
        logger.setLevel(logging.INFO)

    if args['--verbose'] == 2:
        logger.setLevel(logging.DEBUG)
        args['--debug'] = True

    if args['--verbose'] >= 3:
        logger.setLevel(DEBUG_LEVELV_NUM)
        args['--debug'] = True

    if args['--quiet']:
        logger.setLevel(logging.ERROR)

    # print("called with args = %s", args)


def ferryfeed(output=".", clean=False, debug=False, verbose=0, quiet=False):
    """scrape wsdot's ferry feeds, clean them up, write them out"""

    args['--output'] = output
    args['--clean'] = clean
    args['--debug'] = debug
    args['--verbose'] = verbose
    args['--quiet'] = quiet

    process_command_line_args()

    # print("ferryfeed: args: %s" % args)
    # sys.stdout.flush()

   # logger.warning("warning")
   # logger.info("info")
   # logger.debug("debug")
   # logger.debugv("debugv")

    main()


def main():
    """scrape wsdot's ferry feeds, clean them up, write them out"""

    # print("main: args: %s" % args)
    # sys.stdout.flush()

    # logger.warning("warning:")
    # logger.info("info:")
    # logger.debug("debug:")
    # logger.debugv("debugv:")

    # exit()

    logger.info("ferryfeed %s" % __version__)
    logger.debug("Python version: %s", sys.version.split()[0])

    output = args['--output']

    if args['--clean']:
        # logger.warning(
        #     "deleting all .rss and .json files from output %s", output)
        for ext in ['rss', 'json']:
            glob_pattern = output + "/*." + ext
            logger.debug("removing %s", glob_pattern)
            for file in glob(glob_pattern):
                logger.debug("%s", file)
                os.remove(file)

    FerrySystem.initialize()
    feeds = []
    eastward = []
    westward = []
    for route in FerrySystem.routes():
        name = route.basename()
        url = route.rss_url(remaining=False)
        feed = Feed.get(name, route, url)
        feed.write_rss(dir=output)
        feeds.append(feed)
        if route.is_eastward():
            eastward.append(feed)
        if route.is_westward():
            westward.append(feed)

    title = "All WSF Sailings"
    link = "http://www.wsdot.wa.gov/Ferries/Schedule/ScheduleByDate.aspx"
    description = "Sailing times for Washington State Ferries"
    all = Feed.merge(feeds, title, link, description)
    all.write_rss(name="all", dir=output, json=True)

    title = "Seattle Inbound Sailings"
    link = "http://www.wsdot.wa.gov/Ferries/Schedule/ScheduleByDate.aspx"
    description = "WSF sailing times into Seattle"
    all = Feed.merge(eastward, title, link, description)
    all.write_rss(name="eastward", dir=output, json=True)

    title = "Seattle Outward Sailings"
    link = "http://www.wsdot.wa.gov/Ferries/Schedule/ScheduleByDate.aspx"
    description = "WSF sailing times out of Seattle"
    all = Feed.merge(westward, title, link, description)
    all.write_rss(name="westward", dir=output, json=True)


####################

if __name__ == "__main__":

    args.update(docopt(__doc__))

    process_command_line_args()
    main()
