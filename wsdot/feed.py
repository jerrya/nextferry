#!python

"""Feed management: scrape feeds, parse feeds, merge feeds, generate feeds"""

from datetime import datetime
from datetime import timedelta
import json
import logging
from operator import itemgetter
import os
import random

import re
import unittest

import feedgenerator
import feedparser
from pytz import timezone, utc
import requests


logger = logging.getLogger("ferryfeed")


def is_dst(zone):
    """returns True if a timezone is observing daylight savings time"""
    # from http://stackoverflow.com/a/19778845/608970
    tz = timezone(zone)
    now = utc.localize(datetime.utcnow())
    return now.astimezone(tz).dst() != timedelta(0)


class Feed:
    """An RSS feed
    See https://validator.w3.org/feed/docs/rss2.html for details"""

    def __init__(self, name, route=None, subtitle="",
                 url=None, entries=None,
                 link=None, description=None, ttl=10):
        """initialize a feed from either a url or a list of entries)"""

        self.route = route

        self.name = name

        self.link = link
        self.description = description
        self.subtitle = subtitle
        self.ttl = ttl

        self.entries = entries
        self.parse = None
        self.text = None
        self.encoding_bytes = None

        if url:
            self.url = url

            try:
                logger.debug("scraping %s", url)
                r = requests.get(url, timeout=3)
                r.raise_for_status()
            except requests.exceptions.RequestException as e:
                logger.exception(
                    "Exception raised requesting route %s: %s" % (route, e))
            else:
                self.encoding_bytes = r.text[0:3]
                self.text = r.text[3:]

                self.parse = feedparser.parse(self.text)

                self.link = self.parse.feed['link']
                self.description = self.parse.feed['description']
                self.subtitle = self.parse.feed['subtitle']

                self.entries = []
                for item in self.parse.entries:
                    departure, entry = self.fixup_item(item)
                    self.entries.append([departure, entry])

        elif entries:
            self.entries = entries

        logger.debug("Feed: __init__: name %s route %s", name, route)
        if url:
            logger.debug("  url: %.256s", url)
        logger.debug("  entries: %s", len(self.entries))

    def fixup_item(self, item):
        """The wsdot feeds are valid, but spare. Clean them up."""

        # the items are cleaned up with the ultimate intent to make
        # them legit to write out as is in an rss feed. This means
        # when we write them out as json we may need a bit more
        # conversion.

        # feedparser does a great job of making rss and atom feeds
        # equivalent, but atm I'm not interested in the atom fields
        # and they sure garbage up the structure during debugging.
        for cleanme in ['title_detail', 'links']:
            if cleanme in item:
                del item[cleanme]

        logger.debugv("fixup_item: %s", item)

        # 1. better title

        time_parse = item['title']
        fmt = "%m/%d/%Y - %I:%M %p"
        try:
            departure = datetime.strptime(time_parse, fmt)
        except ValueError as e:
            departure = None

        name = self.route.pretty_name()
        fmt = '%I:%M %p %A'
        if departure:
            time = departure.strftime(fmt).lstrip("0").replace(" 0", " ")
            title = time + ", " + name
        else:
            title = name
        item['title'] = title

        # 2. a pubDate

        zone = "America/Los_Angeles"
        tz = timezone(zone)
        departurePT = tz.localize(departure)
        item['pubDate'] = departurePT

        # 3. a description with a google maps image and link

        image = self.route.maps_image_url()
        map = self.route.maps_url()
        terminal = self.route.depart.pretty_name()
        caption = "Driving directions to " + terminal
        directions = '<a href="{}"><img src="{}"></a>' + \
                     '<p><a href="{}">{}</a></p>'
        directions = directions.format(map, image, map, caption)

        details = self.route.departure_details_url()
        status = '<a href="{}">Current {} status</a>'
        status = status.format(details, terminal)

        comment_template = "{}<p>{}</p>"
        comment = comment_template.format(directions, status)

        item['description'] = comment

        # squirrel the map and wsdot links away in the item for
        # inclusion in the json for later use by a js frontend later

        item['map_link'] = map
        item['map_image'] = image
        item['terminal_link'] = details

        # 4. some categories

        category = [self.route.pretty_name()]
        direction = None
        if self.route.is_eastward():
            direction = "eastward"
        elif self.route.is_westward():
            direction = "westward"
        if direction:
            category.append(direction)
        item['categories'] = category

        # 5. A guid
        item['guid'] = "%s:%s" % (self.route, departure.isoformat())

        logger.debugv("fixup_item: -> %s", item)

        return departure, item

    def write_rss(self, dir=".", name=None, json=False, pretty=True):
        """write a feed out as rss and possibly as json too"""

        if name is None:
            name = self.name

        if not os.path.exists(dir):
            os.mkdir(dir)
        filename = "%s/%s.rss.xml" % (dir, name)

        logger.info("writing %s entries into %s", len(self.entries), filename)

        new_feed = feedgenerator.Rss201rev2Feed(
            title=self.name,
            link=self.link,
            subtitle=self.subtitle,
            description=self.description,
            ttl=self.ttl
        )

        for _, entry in self.entries:
            new_feed.add_item(
                title=entry['title'],
                link=entry['link'] + ("&ignore=%s" % random.getrandbits(20)),
                description=entry['description'],
                categories=entry['categories'],
                unique_id=entry['guid'],
                comments=entry['map_link'],
                pubdate=entry['pubDate']
            )

        rss_text = new_feed.writeString('utf-8')
        if pretty:
            rss_text = re.sub(r"><", r">\n    <", rss_text)
            rss_text = re.sub(r"  <(/?)i", r"<\1i", rss_text)
            rss_text = re.sub(r"    <(/?)((ch)|r)", r"<\1\2", rss_text)

        with open(filename, "w") as out:
            out.write(rss_text)

        if json:
            filename = "%s/%s.json" % (dir, name)
            logger.info("writing %s", filename)

            json = self.json()
            with open(filename, "w") as out:
                out.write(json)

    def json(self):
        """returns a json string representation of the feed"""

        items = []
        for _, item in self.entries:
            entry = {}
            for element in ['title', 'link',
                            'map_link', 'map_image', 'terminal_link']:
                if element in item:
                    entry[element] = item[element]

            if 'pubDate' in item:
                d = item['pubDate']
                d = d.strftime("%a, %d %b %Y %H:%M:%S %z")
                entry['pubDate'] = d

            # our feedgenerator doesn't support category domains
            # so a bit of a name munging kluge goes here
            # to make the json representation a bit nicer for a frontend
            if 'categories' in item:
                d = {}
                for cat in item['categories']:
                    if cat == "eastward" or cat == "westward":
                        d['direction'] = cat
                    else:
                        d['route'] = cat
                entry['categories'] = d

            items.append(entry)

        j = {
            "feed": {
                "title": self.name,
                "link": self.link,
                "description": self.description,
                "ttl": self.ttl
            },
            "items": items
        }

        s = json.dumps(j, indent=1)
        return s

    @staticmethod
    def get(name, route, url):
        """return a feed object representing the feed at the url"""
        f = Feed(name, route=route, url=url)
        return f

    @staticmethod
    def merge(feeds, title, link, description):
        """return a new feed from merged feeds, sort by departure time"""

        logger.info("Feed.merge(%s) %s feeds)", title, len(feeds))
        entries = []
        for feed in feeds:
            logger.debug(feed.name)
            entries.extend(feed.entries)
        i = 0
        for entry in entries:
            logger.debugv("presort: %s %s %s: %s", i, entry[0],
                          type(entry[0]), entry[1]['title'])
            i += 1

        schedule = sorted(entries, key=itemgetter(0))

        i = 0
        for entry in schedule:
            logger.debugv("postsort: %s %s: %s", i,
                          entry[0], entry[1]['title'])
            i += 1

        feed = Feed(title, link=link,
                    description=description, entries=schedule)

        logger.debug("Feed.merged %s entries)", len(entries))
        return feed

####################


class TestFeeds(unittest.TestCase):

    # @classmethod
    # def setUpClass(cls):
    #     # print("setUpClass %s" % cls)
    #     # print("initializing ferry system")
    #     FerrySystem.initialize()

    def test_0initialization(self):
        # print("\ntest 0: initialization")
        # routes = FerryRoute.all()
        # for route in routes:
        #     print(route)
        #     print("test 0: route is: %s %s" % (type(route), route))
        # self.assertEqual(8, len(routes))
        pass


if __name__ == "__main__":

    # unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestFeeds)
    unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)
