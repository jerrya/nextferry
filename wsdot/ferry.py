#!python

"""A FerrySystem has FerryTerminals and FerryRoutes"""

import logging
import unittest

logger = logging.getLogger("ferryfeed")

_wsdot_ids = {
    "Port Townsend": 17,
    "Coupeville": 11,
    "Kingston": 12,
    "Edmonds": 8,
    "Seattle": 7,
    "Bainbridge": 3,
    "Bremerton": 4,
    "Mukilteo": 14,
    "Clinton": 5,
    "Fauntleroy": 9,
    "Southworth": 20,
}

_routes = {
    "Port Townsend": ["Coupeville"],
    "Coupeville": ["Port Townsend"],
    "Kingston": ["Edmonds"],
    "Edmonds": ["Kingston"],
    "Seattle": ["Bainbridge", "Bremerton"],
    "Bainbridge": ["Seattle"],
    "Bremerton": ["Seattle"],
    "Mukilteo": ["Clinton"],
    "Clinton": ["Mukilteo"],
    "Fauntleroy": ["Southworth"],
    "Southworth": ["Fauntleroy"],
}

_google_maps_places = {
    "Port Townsend": 'Port+Townsend+Terminal+Port+Townsend+WA',
    "Coupeville": 'Coupeville+Ferry+Terminal+Coupeville+WA',
    "Kingston": 'Kingston+Ferry+Terminal+Kingston+WA',
    "Edmonds": 'Edmonds+Ferry+Terminal+Edmonds+WA',
    "Seattle": 'Seattle+Ferry+Terminal+Seattle+WA',
    "Bainbridge": 'Bainbridge+Island+Terminal+Bainbridge+WA',
    "Bremerton": 'Bremerton+Transportaton+Center+Bremerton+WA',
    "Mukilteo": 'Mukilteo+Ferry+Terminal+Mukilteo+WA',
    "Clinton": 'Clinton+Ferry+Terminal+Clinton+WA',
    "Fauntleroy": 'Fauntleroy+Ferry+Terminal+Fauntleroy+WA',
    "Southworth": 'Southworth+Ferry+Terminal+Southworth+WA',
}

_gmaps_places_url = "https://www.google.com/maps/place/Current+Location/"
_gmaps_directions_url = "https://www.google.com/maps/dir/Current+Location/"
_gmaps_image_url = "https://maps.googleapis.com/maps/api/staticmap?zoom=14&size=200x200&center="

_eastward_routes = [
    "Port Townsend",
    "Kingston",
    "Bainbridge",
    "Bremerton",
    "Clinton",
    "Southworth",
]

_westward_routes = [
    "Coupeville",
    "Edmonds",
    "Seattle",
    "Mukilteo",
    "Fauntleroy",
]

EAST = "eastward"
WEST = "westward"

_wsdot_ferry_schedule_rss = \
    "http://www.wsdot.com/" \
    "Ferries/Schedule/RSSFeeds/RemainingSailingsToday.aspx" \
    "?departingterm={}&arrivingterm={}&onlyremainingtimes={}"

_wsdot_terminal_details_url = \
    "http://www.wsdot.wa.gov/" \
    "ferries/vesselwatch/TerminalDetail.aspx?terminalid={}"


class FerrySystem():

    @staticmethod
    def initialize():
        """initialize terminals and routes in our ferry system"""
        logger.debug("Initializing Ferry System")
        FerryTerminal.reset()
        FerryRoute.reset()
        for terminal, endpoints in _routes.items():
            # lookup the terminals, registering them if need be
            departure = FerryTerminal.lookup(terminal)
            if departure is None:
                departure = FerryTerminal(terminal)
            logger.debugv("departure is %s" % departure)
            for endpoint in endpoints:
                arrival = FerryTerminal.lookup(endpoint)
                if arrival is None:
                    arrival = FerryTerminal(endpoint)
                logger.debugv("arrival is %s" % arrival)

                # register a route for each terminal and endpoint
                route = FerryRoute(terminal, endpoint)
                logger.debug("route %s", route)
        routes = FerryRoute.routes()
        logger.info("%s ferry routes.", len(routes))
        return routes

    @staticmethod
    def reset():
        FerryTerminal.reset()
        FerryRoute.reset()

    @staticmethod
    def routes(directional_filter=None):
        """returns routes in the ferry system"""
        return FerryRoute.routes()


class FerryTerminal:
    """A Ferry Terminal has a terminal number given by wsdot and a name.

    the terminal number is unique within wsdot's system
    the name is assumed unique as well

    Each instance has methods for
    + pretty printing,

    The class has methods for
    + lookup by name
    + lookup by id
    + returning all terminals
    """

    terminals = {}

    def __init__(self, name):
        self.id = _wsdot_ids[name]
        self.name = name
        logger.debugv("FerryTerminal: __init__ %s" % name)

        if name not in FerryTerminal.terminals:
            FerryTerminal.terminals[name] = self

    def __repr__(self):
        return "{}{}".format(self.name, self.id)

    def pretty_name(self):
        return self.name

    @classmethod
    def reset(cls):
        """reset the system entirely"""
        cls.terminals = {}

    @classmethod
    def all(cls):
        """return all terminal instances"""
        return cls.terminals.keys()

    @classmethod
    def lookup(cls, name):
        """return the terminal corresponding to a name"""
        terminal = None
        if name in cls.terminals:
            terminal = cls.terminals[name]
        logger.debugv("FT.lookup %s => %s", name, terminal or "FAILED")
        return terminal

    @classmethod
    def lookup_id(cls, id):
        """return the terminal corresponding to a terminal id"""
        for terminal in cls.terminals.keys():
            if terminal.id == id:
                return terminal
        return None


class FerryRoute:
    """A Ferry Route has a terminal to depart and one to arrive.

    Every FerryRoute Instance has methods for
    + pretty printing,
    + returning rss url

    The class has methods for
    + enumerating routes
    + lookup of a route by terminal names
    + returning the rss url of a route
    """

    _routes = []

    @classmethod
    def reset(cls):
        """reset the system entirely"""
        cls._routes = []

    def __init__(self, depart, arrive):
        logger.debug("Route: __init__ %s -> %s", depart, arrive)
        self.depart = FerryTerminal.lookup(depart)
        if self.depart is None:
            raise ValueError("%s departure terminal not found!" % depart)
        self.arrive = FerryTerminal.lookup(arrive)
        if self.arrive is None:
            raise ValueError("%s arrival terminal not found!" % arrive)
        FerryRoute._routes.append(self)

    def __str__(self):
        name = "%s to %s" % (self.depart.pretty_name(),
                             self.arrive.pretty_name())
        return name

    def __repr__(self):
        name = "%s to: %s" % (self.depart, self.arrive)
        return name

    def pretty_name(self):
        return self.__str__()  # placeholder for later...?

    def uid(self):
        return "%s:%s" % (self.depart.id, self.arrive.id)

    def basename(self):
        """a basename useful for files and urls"""
        departure = self.depart.pretty_name()
        departure = departure.replace(" ", "")
        arrival = self.arrive.pretty_name()
        arrival = arrival.replace(" ", "")
        basename = departure + "-" + arrival
        logger.debug("route basename: %s", basename)
        return basename

    def is_eastward(self):
        return self.depart.name in _eastward_routes

    def is_westward(self):
        return self.depart.name in _westward_routes

    @classmethod
    def routes(cls):
        """returns routes in the ferry system"""
        return list(cls._routes)

    @classmethod
    def lookup(cls, departure=None, arrival=None):
        """return the routes that depart from or arrive at the named terminals.
        May return None, one, or a list of routes"""
        if departure:
            depart = FerryTerminal.lookup(departure)
        if arrival:
            arrive = FerryTerminal.lookup(arrival)

        if depart is None and arrive is None:
            return None

        routes = []

        for route in cls._routes:
            if departure and arrival:
                if route.depart.name == departure \
                   and route.arrive.name == arrival:
                    routes.append(route)
            elif departure:
                if route.depart.name == departure:
                    routes.append(route)
            else:
                if route.arrive.name == arrival:
                    routes.append(route)

        return routes if len(routes) > 1 else route[0]

    def rss_url(self, remaining=True):
        """returns the url of the rss feed for this route"""
        departing = self.depart.id
        arriving = self.arrive.id
        return _wsdot_ferry_schedule_rss \
            .format(departing, arriving, remaining)

    def departure_details_url(self):
        """returns the url of the details page of the departure terminal"""
        departing = self.depart.id
        return _wsdot_terminal_details_url.format(departing)

    def maps_url(self, directions=True):
        """returns a url to google maps"""

        name = self.depart.name
        if directions:
            url = _gmaps_directions_url + _google_maps_places[name]
        else:
            url = _gmaps_places_url + _google_maps_places[name]

        return url

    def maps_image_url(self):
        """returns a url to google maps"""

        name = self.depart.name
        url = _gmaps_image_url + _google_maps_places[name]

        return url

####################


class Test_FerryTerminal(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.info("setUpClass %s", cls)
        logger.info("initializing ferry system")
        FerrySystem.initialize()

    def test_0initialization(self):
        logger.info("test 0: initialization")
        routes = FerryRoute.all()
        for route in routes:
            logger.debug(route)
            logger.debug("test 0: route is: %s %s", type(route), route)
        self.assertEqual(8, len(routes))

    def test_1initialized_terminals(self):
        terminals = FerryTerminal.all()
        for terminal in terminals:
            logger.info(terminal)
        self.assertEqual(7, len(terminals))

    def test_2initialized_routes(self):
        routes = FerryRoute.all()
        for route in routes:
            logger.info(route)
        self.assertEqual(8, len(routes))

    def test_3terminallookups(self):
        pt = FerryTerminal.lookup("PortTownsend")
        self.assertIsNone(pt)
        pt = FerryTerminal.lookup("Port Townsend")
        self.assertEquals("Port Townsend", pt.name)
        self.assertEquals(17, pt.id)

    def test_4routelookups(self):
        ptcoup = FerryRoute.lookup("Port Townsend", "Coupville")
        self.assertIsNone(ptcoup)  # misspelling

        ptcoupe = FerryRoute.lookup("Port Townsend", "Coupeville")
        self.assertIsNotNone(ptcoupe)
        coupept = FerryRoute.lookup("Coupeville", "Port Townsend")
        self.assertIsNotNone(coupept)

    def test_5FerrySystemRoutes(self):
        self.assertEqual(FerrySystem.routes(), FerryRoute.all())


# if __name__ == "__main__":

#     # unittest.main()
#     suite = unittest.TestLoader().loadTestsFromTestCase(TestFerryTerminal)
#     unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)
