#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), "wsdot"))

# from ferryfeed import ferryfeed
# ferryfeed(output="content/ferries")

# SUMMARY_MAX_LENGTH = 0

AUTHOR = u'Jerry Asher'
SITENAME = u'Next Ferry'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = u'English'

OUTPUT_PATH = 'public'  # (OUTPUT) Generated site goes here
DELETE_OUTPUT_DIRECTORY = True  # delete between regenerations

ARTICLE_PATHS = ['posts']

ARTICLE_URL = 'posts/{date:%Y}/{date:%m}{date:%d}/{slug}/'
ARTICLE_SAVE_AS = 'posts/{date:%Y}/{date:%m}{date:%d}/{slug}/index.html'

PAGE_PATHS = ['pages']

STATIC_PATHS = ['images', 'pdfs', 'downloads', 'assets', 'ferries']

DIRECT_TEMPLATES = ['index', 'tags', 'categories',
                    'authors', 'archives']
PAGINATED_DIRECT_TEMPLATES = ['index']
DEFAULT_PAGINATION = 5

# place robots and favicon in the root directory
EXTRA_PATH_METADATA = {
    'assets/rubik-unsolved.png': {'path': '.well-known/acme-challenge/rubik-unsolved.png'},
    'assets/acme-challenge-september-2017.txt': {'path': '.well-known/acme-challenge/ZoZGf2ePgLDb9BrobPc0VrXItwVvPMprpXtD0nmvKdM'},
    'assets/robots.txt': {'path': 'robots.txt'},
    'assets/favicon.ico': {'path': 'favicon.ico'},
}

USE_FOLDER_AS_CATEGORY = False
DEFAULT_CATEGORY = 'blog'
DEFAULT_DATE = 'fs'

DISPLAY_PAGES_ON_MENU = True
DISPLAY_PAGES_ON_SIDEBAR = True
DISPLAY_CATEGORIES_ON_MENU = False

# Feed generation is usually not desired when developing
# FEED_DOMAIN = '//nextferry.ashercodes.com'

TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/%s.atom.xml'
FEED_RSS = 'feeds/rss.xml'

PLUGIN_PATHS = ['plugins']
PLUGINS = [
    'neighbors',
    'sitemap',
    'summary',
    'pelican_fontawesome',
    'pelican_jsfiddle',
    'page_specific_assets',
]

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'weekly',
        'indexes': 'weekly',
        'pages': 'monthly'
    }
}

TYPOGRIFY = True

PYGMENTS_STYLE = 'monokai'

# Blogroll
# LINKS = (
#     ('Tutoring', 'http://tutoring.ashercodes.com'),
#     ('Next Ferry', 'http://nextferry.ashercodes.com'),
# )

# Social widget
SOCIAL = (
    ('<i class="fa fa-linkedin-square fa-fw"></i>',
     'https://linkedin.com/in/jerryasher'),
    ('<i class="fa fa-github-square fa-fw"></i>',
     'https://github/jerryasher'),
    ('<i class="fa fa-twitter fa-fw"></i>', 'https://twitter.com/jerryasher'),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

YEAR_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = 'posts/{date:%Y}/{date:%b}/index.html'

# THEME = "notmyidea" # default
# THEME = "simple"
# THEME = "themes/voidy-bootstrap"
# THEME = "themes/crowsfoot"
# THEME = "themes/elegant"
# THEME = "themes/zurb-F5-basic"
THEME = "themes/materialistic-pelican"

# materialistic-pelican

USE_CDN = False
USER_LOGO_URL = '/images/ferry.jpg'
USER_AVATAR_URL = '/images/captain-ahab.png'

PRIMARY_COLOR = 'Indigo'
ACCENT_COLOR = 'Light Blue'

# PRIMARY_COLOR = 'Cyan'
# ACCENT__COLOR = 'Cyan'
# ACCENT_COLOR = 'Amber'

TWITTER_USERNAME = 'jerryasher'
TWITTER_TWEET_BUTTON = True

GIST_CACHE_LOCATION = "TMPDIR"
