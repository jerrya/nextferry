Title: Colophon
Date: 2016-09-13
Category: General
Status: Hidden
Summary: How the blog is made

This website is a [Pelican](http://blog.getpelican.com/) blog and a work in progress.

The repo for this blog is at:  [https://gitlab.com/jerrya/nextferry](https://gitlab.com/jerrya/nextferry)

