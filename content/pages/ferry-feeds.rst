:title: Ferry Feeds
:date: 2017-01-11
:category: General
:tags: ferry
:summary: ferry information in rss and json formats

============
Ferry Feeds:
============
----------------------------------
A collection of RSS and JSON feeds
----------------------------------

OPML
----

Download and import into your feed reader

* `ferries.opml <{filename}/assets/ferries.opml>`_

RSS
---

* `all ferries <{filename}/ferries/all.rss.xml>`_
* `eastward bound ferries (into Seattle) <{filename}/ferries/eastward.rss.xml>`_
* `westward bound ferries (out of Seattle) <{filename}/ferries/westward.rss.xml>`_

* `Bainbridge-Seattle <{filename}/ferries/Bainbridge-Seattle.rss.xml>`_
* `Bremerton-Seattle <{filename}/ferries/Bremerton-Seattle.rss.xml>`_
* `Clinton-Mukilteo <{filename}/ferries/Clinton-Mukilteo.rss.xml>`_
* `Coupeville-PortTownsend <{filename}/ferries/Coupeville-PortTownsend.rss.xml>`_
* `Edmonds-Kingston <{filename}/ferries/Edmonds-Kingston.rss.xml>`_
* `Fauntleroy-Southworth <{filename}/ferries/Fauntleroy-Southworth.rss.xml>`_
* `Kingston-Edmonds <{filename}/ferries/Kingston-Edmonds.rss.xml>`_
* `Mukilteo-Clinton <{filename}/ferries/Mukilteo-Clinton.rss.xml>`_
* `PortTownsend-Coupeville <{filename}/ferries/PortTownsend-Coupeville.rss.xml>`_
* `Seattle-Bainbridge <{filename}/ferries/Seattle-Bainbridge.rss.xml>`_
* `Seattle-Bremerton <{filename}/ferries/Seattle-Bremerton.rss.xml>`_
* `Southworth-Fauntleroy <{filename}/ferries/Southworth-Fauntleroy.rss.xml>`_

JSON
----

* `all <{filename}/ferries/all.json>`_
* `westward <{filename}/ferries/westward.json>`_
* `eastward <{filename}/ferries/eastward.json>`_

