Title: Geolocation Javacript Example
Date: 2017-10-20

<script>
function getLocation4()
{
  // Check whether browser supports Geolocation API or not
  if (navigator.geolocation) { // Supported
  
    // To add PositionOptions
	
	navigator.geolocation.getCurrentPosition(getPosition4);
  } else { // Not supported
	alert("Oops! This browser does not support HTML Geolocation.");
  }
}
function getPosition4(position)
{
  document.getElementById("location4").innerHTML = 
	  "Latitude: " + position.coords.latitude + "<br>" +
	  "Longitude: " + position.coords.longitude;
}

// To add catchError(positionError) function

</script>

<h1>Finding Me (html geolocation demo) (4)</h1>
<button onclick="getLocation4()">Where am I?</button>
<p id="location4"></p>

<!-- PELICAN_END_SUMMARY -->
