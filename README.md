[![pipeline status](https://gitlab.com/jerrya/nextferry/badges/master/pipeline.svg)](https://gitlab.com/jerrya/nextferry/-/commits/master)

----

This is the [next ferry](https://nextferry.ashercodes.com) website.

It exists to answer several questions of mine:

1. When and where is the next ferry crossing the Puget Sound?
2. When and where is the last ferry leaving Seattle tonight?

WSDOT publishes lots on its website, and they have a very nice Android app, but nowhere are the questions above easily answered.

So this website integrates their published feeds, and publishes new feeds to present the data in a more usable form.

This site is very much a work in progress. The current status is that a rudimentary site has been built, and the WSDOT feeds are scraped each day, cleaned up, and republished.

To do:

1. Build a vue.js "rich" progressive web app for use on mobile and desktop
1. Clean up this site as a fallback for browsers that don't enable chrome

Those feeds can be seen at the ferry feeds page [https://nextferry.ashercodes.com/pages/ferry-feeds/](https://nextferry.ashercodes.com/pages/ferry-feeds/).

You can subscribe to the feeds as they are using any RSS reader (I recommend feedly.com) to see that even now, the feeds are enhanced to provide:

+ A merged sorted timeline of the days trips
+ Times of each sailing
+ A link to google directions from your current location to the ferry terminal
+ A link from each sailing to the terminal status page which can tell you how much room is left on a ferry, or what the terminal wait time is.

As an example, compare WSDOT's published Bremerton Seattle Route with my republished Bremerton Seattle Route

1. [http://www.wsdot.wa.gov/ferries/schedule/RSSFeeds/RemainingSailingsToday.aspx?departingterm=4&arrivingterm=7&onlyremainingtimes=true](http://www.wsdot.wa.gov/ferries/schedule/RSSFeeds/RemainingSailingsToday.aspx?departingterm=4&arrivingterm=7&onlyremainingtimes=true)
1. [https://nextferry.ashercodes.com/ferries/Bremerton-Seattle.rss.xml](https://nextferry.ashercodes.com/ferries/Bremerton-Seattle.rss.xml)

If you subscribe to the WSDOT feeds, you'll find that in most feed readers, the feeds are basically useless. They are sparse, titled poorly, and the links do not go anywhere useful.

There is an OPML file you can use to import all the feeds into your feed reader. Open the link, download it, then import it in your feed reader.

1. [Next Ferry OPML](https://nextferry.ashercodes.com/assets/ferries.opml)

In addition there are three JSON feeds for use by a vue.js progressive web app I am building to much more effectively, much more visually present the data and answer the questions above.

1. [all.json](https://nextferry.ashercodes.com/ferries/all.json) a json of the entire day's sailings
1. [westward.json](https://nextferry.ashercodes.com/ferries/westward.json) a json of the day's westward sailings away from Seattle
1. [eastward.json](https://nextferry.ashercodes.com/ferries/eastward.json) a json of the day's eastward sailings towards Seattle

It is a static website hosted at gitlab, rebuilt early each morning, triggered by an external webhook when WSDOT publishes the day's ferry information.

The first thing that happens is WSDOT's ferry feeds are scraped, cleaned up, integrated with other data sources, and reformatted to make them more useful. And then the pelican static website generator runs and builds the rest of the site.

The code that actually does the scraping can be found here:

[https://gitlab.com/jerrya/ferries](https://gitlab.com/jerrya/ferries)

It too is a work in progress, but at this time, the basics of

+ scraping
+ cleanup
+ republishing

seem to work

It is an object oriented set of python modules broken into three basic pieces:

1. ferry.py which models the wsdot ferry system
2. feed.py which implements feed scraping, parsing and generation
3. ferryfeed which coordinates the whole affair (ie, contains main)

